

/**
 * @file
 * Generate example data for api tests
 */

const Catalog = require('../../dist/api/model/Catalog');
const DashboardClient = require('../../dist/api/model/DashboardClient');
const InputParameters = require('../../dist/api/model/InputParameters');
const Plan = require('../../dist/api/model/Plan');
const Schemas = require('../../dist/api/model/Schemas');
const ServiceBindingSchema = require('../../dist/api/model/ServiceBindingSchema');
const Service = require('../../dist/api/model/Service');
const ServiceInstanceSchema = require('../../dist/api/model/ServiceInstanceSchema');
const ProvisionResponse = require('../../dist/api/model/ProvisionResponse');
const LastOperation = require('../../dist/api/model/LastOperation');
const BindServiceResponse = require('../../dist/api/model/BindServiceResponse');
const VolumeMount = require('../../dist/api/model/VolumeMount');
const Device = require('../../dist/api/model/Device');
const BrokerError = require('../../dist/api/model/BrokerError');

const data = {};

function exampleService () {
  return new Service({
    name: 'service-a',
    id: 'service-id',
    description: 'service A',
    tags: ['tag', 'osb'],
    requires: ['route_forwarding'],
    bindable: true,
    metadata: {
      displayName: 'Service 1',
      provider: {
        name: 'provider name'
      }
    },
    dashboard_client: new DashboardClient({
      id: '7545b455-9cdb-41d2-abf3-3a4bae383a43',
      secret: '277cabb0-XXXX-XXXX-XXXX-7822c0a90e5d',
      redirect_uri: 'http://localhost:1234'
    }),
    plan_updateable: true,
    plans: [new Plan({
      id: 'ab3eb1db-26d7-429a-850a-daeb667e0530',
      name: 'plan-1',
      description: 'description',
      metadata: {
        max_storage_tb: 5,
        costs: [
          {
            amount: {
              usd: 99.0
            },
            unit: 'MONTHLY'
          },
          {
            amount: {
              usd: 0.99
            },
            unit: '1GB of messages over 20GB'
          }
        ],
        bullets: [
          'Shared fake server',
          '5 TB storage',
          '40 concurrent connections'
        ]
      },
      schemas: new Schemas({
        service_instance: new ServiceInstanceSchema({
          create: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          }),
          update: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          })
        }),
        service_binding: new ServiceBindingSchema({
          create: new InputParameters({
            parameters: {
              '$schema': 'http://json-schema.org/draft-04/schema#',
              'type': 'object',
              'billing-account': {
                description: 'Billing account number used to charge use of shared fake server.',
                type: 'string'
              }
            }
          })
        })
      })
    }, new Plan({
      id: '11f48b05-e3f3-4265-bdf7-6a0c58dbc9bf',
      name: 'plan-2',
      description: 'Shared fake Server, 5tb persistent disk, 40 max concurrent connections. 100 async',
      free: false,
      metadata: {
        max_storage_tb: 5,
        costs: [
          {
            amount: {
              usd: 199.0
            },
            unit: 'MONTHLY'
          },
          {
            amount: {
              usd: 0.99
            },
            unit: '1GB of messages over 20GB'
          }
        ],
        bullets: [
          '40 concurrent connections'
        ]
      }
    }))]
  });
}

function exampleCatalog () {
  const services = [exampleService()];
  return new Catalog({services});
}

function exampleBindServiceResponse () {
  const credentials = {
    uri: 'mysql://mysqluser:pass@mysqlhost:3306/dbname',
    username: 'mysqluser',
    password: 'pass',
    host: 'mysqlhost',
    port: 3306,
    database: 'dbname'
  };

  const volumeMount = new VolumeMount({
    driver: 'cephdriver',
    container_dir: '/data/images',
    mode: 'r',
    device_type: 'shared',
    device: new Device({
      volume_id: 'bc2c1eab-05b9-482d-b0cf-750ee07de311',
      mount_config: {
        key: 'value'
      }
    })
  });

  return new BindServiceResponse({
    credentials: credentials,
    syslog_drain_url: 'syslog-drain-url',
    route_service_url: 'route-service-url',
    volume_mounts: [volumeMount]
  });
}

data.BAD_REQUEST = new BrokerError({description: 'Invalid request'});
data.EMPTY_RESPONSE = {};
data.ASYNC_REQUIRED_RESPONSE = {
  error: 'AsyncRequired',
  description: 'This service plan requires client support for asynchronous service operations.'
};

data.CATALOG = exampleCatalog();
data.SERIVCE_INFO = exampleService();
data.OPERATION_IN_PROGRESS_RESPONSE = new LastOperation({operation: 'operation', state: 'in progress'});
data.LAST_OPERATION = new LastOperation({state: 'in progres', description: 'provisioning instance'});

data.PROVISION_RESPONSE = new ProvisionResponse({dashboard_url: 'http://localhost/dashboard', operation: 'operation'});
data.BIND_SERVICE_RESPONSE = exampleBindServiceResponse();

module.exports = data;
