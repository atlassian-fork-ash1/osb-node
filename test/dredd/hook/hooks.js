

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');

hooks.beforeEach((transaction, done) => {
  // set expected status code into request as 'instanceId' so we can return data based on test cases
  transaction.request.uri = transaction.request.uri.replace('instance-id', transaction.expected.statusCode);
  transaction.fullPath = transaction.fullPath.replace('instance-id', transaction.expected.statusCode);

  // set 'serviceId' param
  if (transaction.request.body) {
    const jsonBody = JSON.parse(transaction.request.body);
    if (jsonBody['service_id'] || jsonBody['service_id'] === '') {
      jsonBody['service_id'] = 'service-id';
      transaction.request.body = JSON.stringify(jsonBody);
    }
  }

  done();
});
