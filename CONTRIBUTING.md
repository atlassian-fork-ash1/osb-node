# Contributing


## Code of Conduct

Please see [CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md)


## Contributor License Agreement (CLA)

Before accepting your contributions to this project,
we require you to sign one of the below CLAs as appropriate
(these are external links to DocuSign):

-   [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)

-   [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


## Development

-   install dependencies with `npm install`

-   run tests with `npm run test`

-   run style check with `npm run eslint`

-   run dredd check with `npm run dredd`

-   commit hooks are installed via `husky`
