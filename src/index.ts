import OSB = require('./api/OSB');
import OSBService = require('./api/OSBService');
import BindServiceResponse = require('./api/model/BindServiceResponse');
import BrokerError = require('./api/model/BrokerError');
import Catalog = require('./api/model/Catalog');
import Cost = require('./api/model/Cost');
import DashboardClient = require('./api/model/DashboardClient');
import Device = require('./api/model/Device');
import InputParameters = require('./api/model/InputParameters');
import LastOperation = require('./api/model/LastOperation');
import Plan = require('./api/model/Plan');
import PlanMetadata = require('./api/model/PlanMetadata');
import ProvisionResponse = require('./api/model/ProvisionResponse');
import Schemas = require('./api/model/Schemas');
import Service = require('./api/model/Service');
import ServiceBindingSchema = require('./api/model/ServiceBindingSchema');
import ServiceInstanceSchema = require('./api/model/ServiceInstanceSchema');
import ServiceMetadata = require('./api/model/ServiceMetadata');
import VolumeMount = require('./api/model/VolumeMount');

export = {
  OSB,
  OSBService,
  model: {
    BindServiceResponse,
    BrokerError,
    Catalog,
    Cost,
    DashboardClient,
    Device,
    InputParameters,
    LastOperation,
    Plan,
    PlanMetadata,
    ProvisionResponse,
    Schemas,
    Service,
    ServiceBindingSchema,
    ServiceInstanceSchema,
    ServiceMetadata,
    VolumeMount
  }
};
