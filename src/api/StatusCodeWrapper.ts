class StatusCodeWrapper<T> {
    statusCode?: number;
    data: T | {error: string, description?: string};
}
export = StatusCodeWrapper;
