/**
 * OSB Plan Object
 */
import Schemas = require('./Schemas');
import PlanMetadata = require('./PlanMetadata');

class Plan {
  public id: string;
  public name: string;
  public description: string;
  public metadata: PlanMetadata;
  public free: boolean;
  public bindable: boolean;
  public schemas?: Schemas;

  public constructor({id, name, description, metadata, free = true, bindable, schemas}) {
    this.id = id;
    this.name = name;
    this.description = description;
    if (metadata) {
      this.metadata = new PlanMetadata(metadata);
    }
    this.free = free;
    this.bindable = bindable;
    if (schemas) {
      this.schemas = new Schemas(schemas);
    }
  }
}

export = Plan;
