/**
 * OSB Device Object
 */
class Device {
  public volume_id: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public mount_config: any;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public constructor({volume_id, mount_config}: {volume_id: string, mount_config: any}) {
    this.volume_id = volume_id;
    this.mount_config = mount_config;
  }
}

export = Device;
