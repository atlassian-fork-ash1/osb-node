/**
 * OSB BindResource Object
 */
class BindResource {
  private appGuid: string;
  private route: string;

  constructor ({app_guid, route}: {app_guid: string, route: string}) {
    this.appGuid = app_guid;
    this.route = route;
  }
}

export = BindResource;
