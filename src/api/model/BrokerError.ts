/**
 * OSB Error Object
 */
class BrokerError {
  public error: string;
  public description: string;

  public constructor ({error, description}: {error: string, description: string}) {
    this.error = error;
    this.description = description;
  }
}

export = BrokerError;
