/**
 * OSB Input Parameters Object
 */
class InputParameters {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public parameters: any;

  public constructor({parameters}) {
    this.parameters = parameters;
  }
}

export = InputParameters;
